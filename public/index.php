<!DOCTYPE html>
<html lang="en" class="scroll-smooth" data-theme="light">

<head>
    <title>Arkadia Me</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Arkadia.me">
    <meta name="description" content="Arkadia.me">
    <meta name="keywords" content="Arkadia.me">

    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.arkadia.me/home">
    <meta property="og:title" content="Arkadia.me">
    <meta property="og:description" content="Arkadia.me">
    <meta property="og:image" content="https://www.arkadia.me/assets/images/arkadia-banner.jpg">

    <link rel="Shortcut icon" href="assets/images/favicon.svg">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <script src="assets/js/main.js?<?= time() ?>"></script>

    <!-- tailwind -->
    <link rel="stylesheet" href="assets/css/tailwind.css?<?= time() ?>">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.js"></script>
    <!-- end tailwind -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
        integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="font-Poppins">

    <!-- begin::Navbar -->
    <div class="navbar bg-[#F1F5F9] px-4 fixed z-50 lg:hidden">
        <div class="flex-1 gap-4">
            <button aria-label="button"
                class="btn btn-square btn-ghost bg-slate-300 hover:bg-slate-400 btn-navbar lg:hidden">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24"
                    stroke="#0f172a">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
                </svg>
            </button>
            <a href="?dashboard=dash">
                <img src="assets/images/arkadiame.svg" alt="img" class="block mx-auto max-w-[150px] w-full h-auto ">
            </a>
        </div>
        <div class="flex-none md:gap-2">
            <!-- <div class="form-control order-1 hidden md:block">
                <input type="text" placeholder="Search" class="input input-bordered" />
            </div> -->
            <div class="dropdown dropdown-end order-3 md:order-2">
                <!-- <label tabindex="0" class="btn btn-ghost btn-circle avatar relative">
                    <span class="flex h-3 w-3 absolute top-1 right-1">
                        <span
                            class="animate-ping absolute inline-flex h-full w-full rounded-full bg-red-400 opacity-75"></span>
                        <span class="relative inline-flex rounded-full h-3 w-3 bg-red-500"></span>
                    </span>
                    <i class="fa fa-bell text-slate-600 text-xl"></i>
                </label>
                <div tabindex="0"
                    class="mt-3 shadow menu menu-compact dropdown-content bg-base-100 rounded-md w-80 sm:w-96 overflow-hidden">
                    <div class="text-center text-slate-800 border-b py-3 text-md">Notification</div>
                    <div class="max-h-72 overflow-y-auto">
                        <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                            <div class="avatar">
                                <div class="w-12 h-12 rounded-full">
                                    <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                                </div>
                            </div>
                            <div>
                                <div class="mb-2">
                                    <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                    join
                                    the team
                                </div>
                                <div class="text-slate-400 text-xs">2 MIN AGO</div>
                            </div>
                        </a>
                        <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                            <div class="avatar">
                                <div class="w-12 h-12 rounded-full">
                                    <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                                </div>
                            </div>
                            <div>
                                <div class="mb-2">
                                    <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                    join
                                    the team
                                </div>
                                <div class="text-slate-400 text-xs">2 MIN AGO</div>
                            </div>
                        </a>
                        <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                            <div class="avatar">
                                <div class="w-12 h-12 rounded-full">
                                    <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                                </div>
                            </div>
                            <div>
                                <div class="mb-2">
                                    <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                    join
                                    the team
                                </div>
                                <div class="text-slate-400 text-xs">2 MIN AGO</div>
                            </div>
                        </a>
                        <a href="#" class="hover:bg-slate-100 hover:text-slate-500 flex px-4 py-2 gap-4">
                            <div class="avatar">
                                <div class="w-12 h-12 rounded-full">
                                    <img src="assets/images/dashboard/avatar-2.jpg" alt="img">
                                </div>
                            </div>
                            <div>
                                <div class="mb-2">
                                    <span class="text-primary">Jeremy Rakestraw</span> accepted your invitation to
                                    join
                                    the team
                                </div>
                                <div class="text-slate-400 text-xs">2 MIN AGO</div>
                            </div>
                        </a>
                    </div>
                    <a href="#" class="text-center bg-primary block py-3 font-bold text-white hover:bg-blue-800">View
                        All
                        Notifications</a>
                </div> -->
            </div>
            <div class="dropdown dropdown-end order-2 md:order-3">
                <label tabindex="0" class="btn btn-ghost btn-circle avatar">
                    <div class="w-10 rounded-full">
                        <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img">
                    </div>
                </label>
                <ul tabindex="0" class="mt-3 shadow menu menu-compact dropdown-content bg-base-100 rounded-md w-64">
                    <li class="bg-primary text-white block px-4 py-2">
                        <h6 class="text-lg font-bold p-0 hover:bg-transparent cursor-default">John Doe</h6>
                        <h6 class="p-0 hover:bg-transparent cursor-default">Available</h6>
                    </li>
                    <li>
                        <a class="py-3 text-slate-600">
                            <i class="fa fa-user"></i>
                            Account
                        </a>
                    </li>
                    <li>
                        <a class="py-3 text-slate-600">
                            <i class="fa fa-cog"></i>
                            Setting
                        </a>
                    </li>
                    <li>
                        <a href="index.php" class="py-3 text-slate-600">
                            <i class="fa fa-power-off"></i>
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end::Navbar -->

    <div class="drawer drawer-mobile">
        <input id="my-drawer-2" type="checkbox" class="drawer-toggle" aria-label="input" />
        <div class="drawer-content items-center justify-center relative bg-[#F1F5F9]">
            <ul
                class="bg-white menu lg:hidden p-4 text-[#33333] fixed left-0 right-0 top-[67px] w-full items-start z-[10] transition duration-300 -translate-y-full navbar-menu">
                <!-- <div class="p-4 w-full">
                    <h6 class="font-bold">DASHBOARD</h6>
                </div> -->
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Beranda
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Data Diri
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Redeem
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Voucher
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Riwayat
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Insight
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Riwayat Event
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Info
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Status Tulisan
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Status Video
                    </a>
                </li>
                <li class="w-full">
                    <a href="?dashboard=dash" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Bantuan
                    </a>
                </li>
            </ul>

            <main class="px-6 py-8 mt-16 lg:max-w-[1024px] lg:w-full lg:m-auto">
                <!-- begin::Header -->
                <header class="mb-8 flex justify-between items-center">
                    <img src="assets/images/yoursay.svg" alt="img" class="mb-3" width="150" height="auto">
                    <div class="flex gap-5">
                        <label tabindex="0" class="btn btn-ghost btn-circle avatar relative">
                            <span class="flex h-3 w-3 absolute top-1 right-1">
                                <span
                                    class="animate-ping absolute inline-flex h-full w-full rounded-full bg-red-400 opacity-75"></span>
                                <span class="relative inline-flex rounded-full h-3 w-3 bg-red-500"></span>
                            </span>
                            <i class="fa fa-bell text-slate-600 text-xl"></i>
                        </label>

                        <div class="border border-[#000] rounded-full pt-3 px-3 hidden sm:block">Tulis Sekarang</div>
                        <div class="border border-[#000] rounded-full pt-3 px-3 hidden sm:block">Buat Video</div>
                        <div class="border border-[#000] rounded-full pt-3 px-3 ">Logout</div>
                    </div>
                </header>
                <!-- end::Header -->

                <!-- begin::Content -->
                <p class="text-[#F15A29] font-bold">Anda terdaftar pada akun arkadia.me dan otomatis anda terdaftar
                    sebagai member serbada & iklandisini</p>
                <div class="bg-[#333333] text-white py-3 px-5 font-bold text-xl rounded-full mt-4">Beranda</div>
                <div>
                    <div class="flex justify-center items-center py-10">
                        <h1 class="text-xl font-bold text-black">
                            Event & Promo
                        </h1>
                    </div>


                    <div class="lg:max-w-[1024px] lg:w-full lg:m-auto">
                        <div class="flex flex-wrap">

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>


                <div>
                    <div class="flex justify-center items-center py-10">
                        <h1 class="text-xl font-bold text-black">
                            Post Event
                        </h1>
                    </div>

                    <div class="lg:max-w-[1024px] lg:w-full lg:m-auto">
                        <div class="flex flex-wrap">

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div>
                    <div class="flex justify-center items-center py-10">
                        <h1 class="text-xl font-bold text-black">
                            Top Article of the Week
                        </h1>
                    </div>

                    <div class="lg:max-w-[1024px] lg:w-full lg:m-auto">
                        <div class="flex flex-wrap">

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <div class="max-w-[238px] mx-auto">
                                    <img src="https://source.unsplash.com/WLUHO9A_xik/238x144" alt="img" width="238"
                                        height="144"
                                        class="max-w-[238px] w-full h-auto m-auto block rounded-2xl object-cover">
                                    <span class="block text-[#828282] text-[12px] mt-5 ">Senin, 08 Maret 2021 | 13:40
                                        WIB</span>
                                    <p class="text-lg "
                                        style="overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; ">
                                        6 Mantan Artis Cilik Ini Pernah Ngetop Berkat Aktingnya</p>
                                    <a href="#">
                                        <span class="text-[#EB5757] text-sm italic ">Link: www.bekraf.go.id</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div>
                    <div class="flex justify-center items-center py-10">
                        <h1 class="text-xl font-bold text-black">
                            Top Writer of the Week
                        </h1>
                    </div>

                    <div class="lg:max-w-[1024px] lg:w-full lg:mx-auto">
                        <div class="flex flex-wrap">

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <img src="https://source.unsplash.com/WLUHO9A_xik/162x162"
                                    class="rounded-full block w-full max-w-[162px] mx-auto" width="162" height="162">
                                <div class="max-w-[161px] mx-auto mb-10">
                                    <span class="block text-black text-xl font-bold mt-5 text-center ">Fitri
                                        Suciati</span>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <img src="https://source.unsplash.com/WLUHO9A_xik/162x162"
                                    class="rounded-full block w-full max-w-[162px] mx-auto" width="162" height="162">
                                <div class="max-w-[161px] mx-auto mb-10">
                                    <span class="block text-black text-xl font-bold mt-5 text-center ">Fitri
                                        Suciati</span>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <img src="https://source.unsplash.com/WLUHO9A_xik/162x162"
                                    class="rounded-full block w-full max-w-[162px] mx-auto" width="162" height="162">
                                <div class="max-w-[161px] mx-auto mb-10">
                                    <span class="block text-black text-xl font-bold mt-5 text-center ">Fitri
                                        Suciati</span>
                                </div>
                            </div>

                            <div class="lg:w-3/12 md:w-6/12 w-6/12 p-4">
                                <img src="https://source.unsplash.com/WLUHO9A_xik/162x162"
                                    class="rounded-full block w-full max-w-[162px] mx-auto" width="162" height="162">
                                <div class="max-w-[161px] mx-auto mb-10">
                                    <span class="block text-black text-xl font-bold mt-5 text-center ">Fitri
                                        Suciati</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end::Content -->
            </main>

            <!-- begin::Footer -->
            <div id="footer">
                <p class="bg-[#333333] text-white text-center text-sm py-5">© 2023 suara.com - All Rights Reserved.</p>
            </div>
            <!-- end::Footer -->

        </div>
        <div class="drawer-side lg:z-50">
            <label for="my-drawer-2" class="drawer-overlay" aria-label="input"></label>
            <ul class="menu p-4 w-80  text-[#333333] bg-white">
                <!-- <div class="p-4 pt-16">
                    <h6 class="font-bold">DASHBOARD</h6>
                </div> -->
                <li>
                    <a href="?dashboard=dash">
                        <img src="https://source.unsplash.com/WLUHO9A_xik/147x147"
                            class="rounded-full block w-full max-w-[147px] mx-auto" width="147" height="147">
                    </a>
                    <div class="w-full max-w-[147px] mx-auto text-lg font-bold">Username</div>
                </li>
                <li>
                    <div class="w-full max-w-[147px] mx-auto">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_38)">
                                <path
                                    d="M13.96 9.24C14.12 8.64 15 8.16 15 7.52C15 6.88 14.12 6.36 13.96 5.8C13.8 5.2 14.28 4.32 14 3.8C13.68 3.2 12.68 3.2 12.24 2.76C11.8 2.32 11.8 1.32 11.24 1C10.72 0.68 9.84 1.2 9.24 1.04C8.64 0.88 8.12 0 7.52 0C6.88 0 6.36 0.88 5.8 1.04C5.2 1.2 4.32 0.72 3.8 1C3.2 1.32 3.2 2.32 2.76 2.76C2.32 3.2 1.32 3.2 1 3.76C0.68 4.28 1.2 5.16 1.04 5.76C0.88 6.36 0 6.88 0 7.52C0 8.16 0.88 8.68 1.04 9.24C1.2 9.84 0.72 10.72 1 11.24C1.32 11.76 2.32 11.8 2.76 12.24C3.2 12.68 3.2 13.68 3.76 14C4.28 14.32 5.16 13.8 5.76 13.96C6.36 14.12 6.84 15 7.48 15C8.12 15 8.64 14.12 9.2 13.96C9.8 13.8 10.68 14.28 11.2 14C11.72 13.68 11.76 12.68 12.2 12.24C12.64 11.8 13.64 11.8 13.96 11.24C14.32 10.72 13.8 9.84 13.96 9.24Z"
                                    fill="#333333" />
                                <path
                                    d="M7.79998 3.68L8.83998 5.8C8.87998 5.88 8.95998 5.96 9.07998 5.96L11.44 6.32C11.72 6.36 11.8 6.68 11.6 6.88L9.91998 8.52C9.83998 8.6 9.79998 8.68 9.83998 8.8L10.24 11.12C10.28 11.36 9.99998 11.56 9.75998 11.44L7.67998 10.32C7.59998 10.28 7.47998 10.28 7.39998 10.32L5.31998 11.44C5.07998 11.56 4.79998 11.36 4.83998 11.12L5.23998 8.8C5.23998 8.68 5.23998 8.6 5.15998 8.52L3.47998 6.88C3.19998 6.68 3.31998 6.36 3.59998 6.32L5.95998 5.96C6.07998 5.96 6.15998 5.88 6.19998 5.8L7.19998 3.68C7.31998 3.44 7.67998 3.44 7.79998 3.68Z"
                                    fill="white" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_38">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        50 Point
                    </div>
                </li>
                <li>
                    <img src="assets/images/whatsapp.png" width="250" height="auto" class="mx-auto block">
                </li>
                <li>
                    <img src="assets/images/telegram.png" width="250" height="auto" class="mx-auto block">
                </li>
                <li>
                    <a href="?dashboard=dash" class="py-3 rounded-sm ">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Beranda
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>

                        Data Diri
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Redeem
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Voucher
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Riwayat
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Insight
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Riwayat Event
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Info
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Status Tulisan
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Status Video
                    </a>
                </li>
                <li>
                    <a href="?dashboard=dash-campaign" class="py-3 rounded-sm">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1_79)">
                                <path
                                    d="M11.5886 9.59593H10.1338C9.78261 9.59593 9.48161 9.74745 9.3311 10.0505L8.27759 11.5151C7.87625 12.0707 7.02341 12.0707 6.62207 11.5151L5.61873 10.0505C5.41806 9.79795 5.11706 9.59593 4.76589 9.59593H3.3612C1.50502 9.59593 0 11.1111 0 12.9798V14.4949C0 14.798 0.200669 15 0.501672 15H14.4983C14.7993 15 15 14.798 15 14.4949V12.9798C15 11.1111 13.495 9.59593 11.5886 9.59593ZM7.47492 8.3333C9.78261 8.3333 11.5886 6.46462 11.5886 4.19189C11.5886 1.86866 9.73244 0.0504761 7.47492 0.0504761C5.16722 0.0504761 3.3612 1.91916 3.3612 4.19189C3.3612 6.46462 5.21739 8.3333 7.47492 8.3333Z"
                                    fill="#333333" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1_79">
                                    <rect width="15" height="15" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        Bantuan
                    </a>
                </li>
            </ul>

        </div>
    </div>

</body>

</html>

<script>
document.addEventListener("DOMContentLoaded", function() {
    setTimeout(() => {
        window.scrollTo(0, 0);
    }, 20000);
    document.querySelector(".btn-navbar").addEventListener("click", function() {
        document
            .querySelector(".navbar-menu")
            .classList.toggle("-translate-y-full");
    });
});
</script>

<script>
$(document).ready(function() {
    jQuery('img').each(function() {
        jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
    });
});
</script>